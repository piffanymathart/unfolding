package Unfolding;

import FlaggedProjection.ProjectionManager;
import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;
import Polyhedron.INormalManager;
import Polyhedron.IPolyhedron;
import Polyhedron.NormalManager;
import Polyhedron.PolyhedronHelper;
import Rotater.Rotater;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Stack;

/**
 * Manages the unfolding of a polyhedron given a tree structure of the unfolding.
 */
class UnscaledUnfoldingManager {

	/**
	 * Manages normal vector calculations
	 */
	private INormalManager m_normalManager = new NormalManager();

	/**
	 * Manages quaternion rotations.
	 */
	private Rotater m_rotater = new Rotater();

	/**
	 * Computes the unfolding transformation matrices for each face of the polyhedron given the tree structure of
	 * the unfolding.
	 * @param polyhedron The polyhedron to unfold.
	 * @param netTree The tree structure describing the unfolding.
	 * @return The set of 4x4 affine transformations specifying the unfolding of each face.
	 */
	Matrix4d[] getUnfoldingTransforms(IPolyhedron polyhedron, INode netTree) {

		// create and initialize node stack
		Stack<INode> nodeStack = new Stack<>();
		nodeStack.add(netTree);

		// create and initialize transform stack
		Stack<Matrix4d> transformStack = new Stack<>();
		transformStack.add(getIdentity());

		// create the transform list (initialized with nulls) to return
		Matrix4d[] transformList = new Matrix4d[polyhedron.getFaceIndices().length];

		// perform depth first search via the stacks
		while (!nodeStack.isEmpty()) {

			// get the current node, its index, and its parent transform
			INode currNode = nodeStack.pop();
			Matrix4d parentTransform = transformStack.pop();

			// unfold from parent
			Matrix4d currTransform = getProduct(
				parentTransform, // parent transform
				getNodeTransform(currNode, polyhedron) // update transform
			);

			// save the current transformation
			transformList[currNode.getValue()] = currTransform;

			// add children to stack along with the current transform
			for(INode child : currNode.getChildren()) {
				nodeStack.add(child);
				transformStack.add(currTransform);
			}
		}

		return transformList;
	}

	/**
	 * Gets the transform associated with unfolding a face/node with respect to its parent. If it does not have
	 * a parent, then the transform flattens the face onto a 2D plane.
	 * @param currNode The node representing the face to unfold/flatten.
	 * @param polyhedron The polyhedron.
	 * @return The transform associated with the unfolding/flattening.
	 */
	private Matrix4d getNodeTransform(INode currNode, IPolyhedron polyhedron) {

		INode parentNode = currNode.getParent();
		IPolygon3d currFace = polyhedron.getFace(currNode.getValue());

		// if current node is root, then simply flatten it
		if (parentNode == null) {
			return getFlattenTransform(currFace);
		}
		// if current node has a parent, then unfold it
		else {
			// try to get a pivot edge and throw exception if not found
			ILine3d pivotEdge = getPivotEdge(polyhedron, currNode);
			if (pivotEdge == null) throw new RuntimeException("no pivot edge for node " + currNode.getValue());
			// compute unfold transform
			return getUnfoldTransform(currFace, polyhedron.getFace(parentNode.getValue()), pivotEdge);
		}
	}

	/**
	 * Gets the transform associated with flattening a face onto a 2D plane.
	 * @param face The face to flatten.
	 * @return The transform associated with the flattening.
	 */
	private Matrix4d getFlattenTransform(IPolygon3d face) {

		Vector3d edgeVec = getNormalizedVector(face.get(0), face.get(1));
		Vector3d normal = m_normalManager.getNormal(face);
		Vector3d crossProd = getCrossProduct(edgeVec, normal);

		return new ProjectionManager().computeChangeOfBasisMatrix( face.get(0), edgeVec, crossProd );
	}

	/**
	 * Gets the pivot edge that unfolds a face/node from its parent.
	 * @param polyhedron The polyhedron that contains the face.
	 * @param childNode The node representing the face to unfold.
	 * @return The pivot edge.
	 */
	private ILine3d getPivotEdge(IPolyhedron polyhedron, INode childNode) {

		INode parentNode = childNode.getParent();
		if(parentNode == null) return null;

		return new PolyhedronHelper().getSharedEdge(polyhedron, childNode.getValue(), parentNode.getValue());
	}

	/**
	 * Gets the transform associated with unfolding a face/node with respect to its parent.
	 * @param childFace The face to unfold.
	 * @param parentFace The face from which to unfold.
	 * @param edge The pivot edge where the unfolding occurs.
	 * @return The transform associated with the unfolding.
	 */
	private Matrix4d getUnfoldTransform(IPolygon3d childFace, IPolygon3d parentFace, ILine3d edge) {

		// get change of basis matrix from child to unit 2d plane

		Vector3d edgeVec = getNormalizedVector(edge);

		// get change of basis matrix for parent

		Vector3d parentOutward = getPerpendicularVector(parentFace, edgeVec);
		Vector3d childInward = getPerpendicularVector(childFace, edgeVec);

		double dotProd = childInward.dot(parentOutward);
		double rotAngle = Math.acos(dotProd);

		return m_rotater.createRotationMatrix(edgeVec, rotAngle, edge.getP1());
	}

	/**
	 * Gets the vector defined by two points.
	 * @param p1 The first point.
	 * @param p2 The second point.
	 * @return The vector.
	 */
	private Vector3d getNormalizedVector(Point3d p1, Point3d p2) {
		Vector3d v = new Vector3d(p2);
		v.sub(p1);
		v.normalize();
		return v;
	}

	/**
	 * Gets the vector defined by the line.
	 * @param line The line.
	 * @return The vector.
	 */
	private Vector3d getNormalizedVector(ILine3d line) {
		Vector3d v = new Vector3d(line.getP2());
		v.sub(line.getP1());
		v.normalize();
		return v;
	}

	/** get the outward vector that lies on the polygon plane and is perpendicular to the vector.
	 * @param polygon The polygon on the plane.
	 * @param vec The vector.
	 * @return The outward vector lying in the polygon plane that is perpendicular to the input vector.
	 */
	private Vector3d getPerpendicularVector(IPolygon3d polygon, Vector3d vec) {

		Vector3d normal = m_normalManager.getNormal(polygon);
		return getCrossProduct(normal, vec);
	}

	/**
	 * Gets the cross product of two vectors.
	 * @param v1 The first vector.
	 * @param v2 The second vector.
	 * @return The vector.
	 */
	private Vector3d getCrossProduct(Vector3d v1, Vector3d v2) {
		Vector3d crossProd = new Vector3d();
		crossProd.cross(v1, v2);
		crossProd.normalize();
		return crossProd;
	}

	/**
	 * Gets the product of two matrices.
	 * @param m1 The first matrix.
	 * @param m2 The second matrix.
	 * @return The matrix product.
	 */
	private Matrix4d getProduct(Matrix4d m1, Matrix4d m2) {
		Matrix4d prod = new Matrix4d(m1);
		prod.mul(m2);
		return prod;
	}

	/**
	 * Gets the identity matrix.
	 * @return The 4x4 affine tranformation identity matrix.
	 */
	private Matrix4d getIdentity() {
		Matrix4d identity = new Matrix4d();
		identity.setIdentity();
		return identity;
	}
}
