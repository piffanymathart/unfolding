package Unfolding;

import Geometry.Polygons.IPolygon2d;
import Geometry.Polygons.Polygon2d;
import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;
import Transformer.Transformer;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import java.awt.*;

/**
 * A polyhedron that contains an unfolding scheme to a net.
 */
public class UnfoldablePolyhedron extends ColouredPolyhedron implements IUnfoldablePolyhedron {

	/**
	 * The node/tree that specifies how the polyhedron unfolds to its net.
	 */
	private INode m_netTree;

	/**
	 * Manages unfolding calculations.
	 */
	private UnfoldingManager m_unfoldingManager = new UnfoldingManager();

	/**
	 * The default constructor.
	 */
	public UnfoldablePolyhedron() {
		super();
		m_netTree = null;
	}

	/**
	 * The copy constructor.
	 * @param unfoldablePolyhedron The polyhedron to copy from.
	 */
	public UnfoldablePolyhedron(IUnfoldablePolyhedron unfoldablePolyhedron) {

		super(unfoldablePolyhedron);
		setNetTree(unfoldablePolyhedron.getNetTree());
	}

	/**
	 * Constructs an unfoldable polyhedron from vertices, face indices, and a tree structure
	 * specifying the net unfolding.
	 * @param vertices The polyhedron's vertices.
	 * @param edgeIndices An array of edge indices, each element being two integers corresponding to
	 * the vertex indices of the end points of the edge.
	 * @param faceIndices An array of face indices, each element being an array of integers corresponding to
	 * vertex indices going counterclockwise around a face, as seen from the outside of the polyhedron.
	 * @param faceColours An array of face colours.
	 * @param netTree The root node of a tree representing the net structure of the unfolding.
	 */
	public UnfoldablePolyhedron(Point3d[] vertices, int[][] edgeIndices, int[][] faceIndices, Color[] faceColours, INode netTree) {

		super(vertices, edgeIndices, faceIndices, faceColours);
		setNetTree(netTree);
	}

	/**
	 * Constructs an unfoldable polyhedron from a polyhedron and a tree structure
	 * specifying the net unfolding.
	 * @param polyhedron The polyhedron.
	 * @param netTree The root node of a tree representing the net structure of the unfolding.
	 */
	public UnfoldablePolyhedron(IColouredPolyhedron polyhedron, INode netTree) {
		super(polyhedron);
		setNetTree(netTree);
	}

	/**
	 * Gets the tree representing the net unfolding.
	 * @return The tree representing the net unfolding.
	 */
	public INode getNetTree() {
		return m_netTree==null ? null : new Node(m_netTree);
	}

	/**
	 * Sets the tree representing the net unfolding.
	 * @param netTree The tree representing the net unfolding.
	 */
	public void setNetTree(INode netTree) {
		m_netTree = (netTree==null) ? null : new Node(netTree);
	}

	/**
	 * Gets the unfolding transforms fitted to the width and height, whichever is a tighter fit.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitted unfolding transforms.
	 */
	public Matrix4d[] getUnfoldingTransforms(int width, int height) {

		return m_unfoldingManager.getUnfoldingTransforms(this, m_netTree, width, height);
	}

	/**
	 * Gets the unfolding fitted to the width and height, whichever is a tighter fit.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitted unfolding.
	 */
	public IPolygon2d[] getUnfolding(int width, int height) {

		Matrix4d[] unfoldingTransforms = m_unfoldingManager.getUnfoldingTransforms(this, m_netTree, width, height);
		return new UnfoldingManager().unfoldFaces(this, unfoldingTransforms);
	}

	/**
	 * Checks if the object is equal to the unfoldable polyhedron.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the unfoldable polyhedron.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!IUnfoldablePolyhedron.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IUnfoldablePolyhedron) obj);
	}

	/**
	 * Checks if the two unfoldable polyhedra are equal.
	 * @param unfoldablePolyhedron The unfoldable polyhedron with which to compare.
	 * @return Whether the unfoldable polyhedra are equal.
	 */
	private boolean equals(IUnfoldablePolyhedron unfoldablePolyhedron) {

		if(!super.equals(unfoldablePolyhedron)) return false;

		INode netTree = unfoldablePolyhedron.getNetTree();
		if(m_netTree==null) {
			return netTree==null;
		}
		else {
			return netTree!=null && m_netTree.equals(netTree);
		}
	}
}
