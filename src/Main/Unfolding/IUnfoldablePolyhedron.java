package Unfolding;

import Geometry.Polygons.IPolygon2d;
import Polyhedron.IColouredPolyhedron;

import javax.vecmath.Matrix4d;

/**
 * A polyhedron that contains an unfolding scheme to a net.
 */
public interface IUnfoldablePolyhedron extends IColouredPolyhedron {

	/**
	 * Gets the tree representing the net unfolding.
	 * @return The tree representing the net unfolding.
	 */
	INode getNetTree();

	/**
	 * Sets the tree representing the net unfolding.
	 * @param netTree The tree representing the net unfolding.
	 */
	void setNetTree(INode netTree);

	/**
	 * Gets the unfolding transforms fitted to the width and height, whichever is a tighter fit.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitted unfolding transforms.
	 */
	Matrix4d[] getUnfoldingTransforms(int width, int height);

	/**
	 * Gets the unfolding fitted to the width and height, whichever is a tighter fit.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitted unfolding.
	 */
	IPolygon2d[] getUnfolding(int width, int height);
}
