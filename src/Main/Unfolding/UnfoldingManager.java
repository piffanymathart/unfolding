package Unfolding;

import Geometry.Polygons.IPolygon2d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon2d;
import Polyhedron.IPolyhedron;
import Transformer.ITransformer;
import Transformer.Transformer;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages the fitting of a polyhedral unfolding onto a page.
 */
class UnfoldingManager extends UnscaledUnfoldingManager {

	/**
	 * Manages the geometric transformations.
	 */
	private ITransformer m_transformer = new Transformer();

	/**
	 * Computes the unfolding transforms that include scaling to width and height, whichever is tighter.
	 * @param polyhedron The polyhedron to unfold.
	 * @param netTree The tree structure representing the unfolding.
	 * @param width The width.
	 * @param height The height.
	 * @return The unfolding transforms that include scaling to width and height, whichever is tighter.
	 */
	Matrix4d[] getUnfoldingTransforms(IPolyhedron polyhedron, INode netTree, int width, int height) {

		Matrix4d[] unfoldingTransforms = super.getUnfoldingTransforms(polyhedron, netTree);
		scaleToFit(unfoldingTransforms, polyhedron, width, height);
		return unfoldingTransforms;
	}

	/**
	 * Scales the unfolding transforms to fit the width and height, whichever is tighter.
	 * @param unfoldingTransforms The unscaled unfolding transforms.
	 * @param polyhedron The polyhedron to unfold.
	 * @param width The width.
	 * @param height The height.
	 */
	private void scaleToFit(Matrix4d[] unfoldingTransforms, IPolyhedron polyhedron, int width, int height) {

		IPolygon2d[] unfolding = unfoldFaces(polyhedron, unfoldingTransforms);
		double[] bb = BoundingBoxHelper.getBoundingBox(unfolding);
		Matrix4d fitMatrix = to4D(FitHelper.fitToRectMatrix(bb, width, height));
		for(Matrix4d transform : unfoldingTransforms) {
			if(transform==null) continue;
			transform.mul(fitMatrix, transform);
		}
	}

	/**
	 * Unfolds the faces of a polyhedron to 2D polygons, without scaling to width and height.
	 * @param polyhedron The polyhedron to unfold.
	 * @param unfoldingTransforms The unfolding transformations.
	 * @return The unfolded faces, not scaled to width and height.
	 */
	public IPolygon2d[] unfoldFaces(IPolyhedron polyhedron, Matrix4d[] unfoldingTransforms) {

		IPolygon3d[] faces = polyhedron.getFaces();
		List<IPolygon2d> unfolding = new ArrayList<>();

		for(int i=0; i<unfoldingTransforms.length; i++) {
			Matrix4d unfoldingTransform = unfoldingTransforms[i];
			if(unfoldingTransform == null) continue;
			IPolygon2d unfoldedFace = unfoldFace(faces[i], unfoldingTransforms[i]);
			unfolding.add(unfoldedFace);
		}
		return unfolding.toArray(new IPolygon2d[unfolding.size()]);
	}

	/**
	 * Unfolds a 3D face to 2D.
	 * @param face The face to unfold.
	 * @param unfoldingTransform The unfolding transformation.
	 * @return The unfolded face, not scaled to width and height.
	 */
	private IPolygon2d unfoldFace(IPolygon3d face, Matrix4d unfoldingTransform) {
		Point3d[] verts = m_transformer.transform(unfoldingTransform, face.getVertices());
		return new Polygon2d(m_transformer.to2D(verts));
	}

	/**
	 * Converts a 3x3 affine transformation to 4x4, while keeping z-value zero.
	 * @param m The 3x3 affine transformation.
	 * @return The corresponding 4x4 affine transformation that keeps z-value zero.
	 */
	private Matrix4d to4D(Matrix3d m) {
		return new Matrix4d(
			m.m00, m.m01, 0, m.m02,
			m.m10, m.m11, 0, m.m12,
			0, 0, 0, 0,
			0, 0, 0, 1
		);
	}
}
