package Unfolding;

import javax.vecmath.Matrix3d;

/**
 * Helps fitting to width or height, whichever is a tight fit.
 */
class FitHelper {

	/**
	 * Computes the fitting matrix to fit a set of 2D polygons to the width and height, whichever is a tighter fit.
	 * @param bb The bounding box needing fitting.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitting matrix.
	 */
	static Matrix3d fitToRectMatrix(double[] bb, int width, int height) {
		double facesAspect = (bb[1]-bb[0])/(bb[3]-bb[2]);
		double rectAspect = (double)width/(double)height;
		if(facesAspect > rectAspect) return fitToWidthMatrix(bb, width, height);
		return fitToHeightMatrix(bb, width, height);
	}

	/**
	 * Computes the fitting matrix to fit a bounding box to the height.
	 * @param bb The bounding box.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitting matrix.
	 */
	private static Matrix3d fitToHeightMatrix(double[] bb, double width, double height) {
		double shapeWidth = height/(bb[3]-bb[2])*(bb[1]-bb[0]);
		double[] newBB = {(width-shapeWidth)/2.0, width-(width-shapeWidth)/2.0, 0, height};
		return getRefitTransform(bb, newBB);
	}

	/**
	 * Computes the fitting matrix to fit a bounding box to the width.
	 * @param bb The bounding box.
	 * @param width The width.
	 * @param height The height.
	 * @return The fitting matrix.
	 */
	private static Matrix3d fitToWidthMatrix(double[] bb, double width, double height) {
		double shapeHeight = width/(bb[1]-bb[0])*(bb[3]-bb[2]);
		double[] newBB = {0, width, (height-shapeHeight)/2.0, height-(height-shapeHeight)/2.0};
		return getRefitTransform(bb, newBB);
	}

	/**
	 * Computes the transformation matrix that stretches objects in the old viewport to fit the new viewport.
	 * @param oldViewport The original 2D viewport given by [leftX, rightX, topY, bottomY].
	 * @param newViewport The new 2D viewport given by given by [leftX, rightX, topY, bottomY].
	 * @return The 3x3 affine transformation matrix that defines the rescaling.
	 */
	private static Matrix3d getRefitTransform(double[] oldViewport, double[] newViewport) {

		// validate parameters

		double xDiff1 = oldViewport[1] - oldViewport[0];
		if(xDiff1==0) throw new RuntimeException("Viewport cannot have zero width.");

		double yDiff1 = oldViewport[3] - oldViewport[2];
		if(yDiff1==0) throw new RuntimeException("Viewport cannot have zero height.");

		double xDiff2 = newViewport[1] - newViewport[0];
		if(xDiff2==0) throw new RuntimeException("Viewport cannot have zero width.");

		double yDiff2 = newViewport[3] - newViewport[2];
		if(yDiff2==0) throw new RuntimeException("Viewport cannot have zero height.");

		// rescale from oldViewport to [0,1]x[0,1]

		Matrix3d mat1 = new Matrix3d(
			1.0 / xDiff1, 0, -oldViewport[0] / xDiff1,
			0, 1.0 / yDiff1, -oldViewport[2] / yDiff1,
			0, 0, 1
		);

		// rescale from [0,1]x[0,1] to newViewport

		Matrix3d mat2 = new Matrix3d(
			xDiff2, 0, newViewport[0],
			0, yDiff2, newViewport[2],
			0, 0, 1
		);

		// apply the composite transform

		Matrix3d matProd = new Matrix3d();
		matProd.mul(mat2, mat1);

		return matProd;
	}
}
