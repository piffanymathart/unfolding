package Unfolding;

import java.util.List;

/**
 * An integer-valued node with pointers to its parent (possibly null) and its children (possibly empty).
 */
public interface INode {

	/**
	 * Gets the parent node.
	 * @return The parent node.
	 */
	INode getParent();

	/**
	 * Gets the list of child nodes.
	 * @return The list of child nodes.
	 */
	List<INode> getChildren();

	/**
	 * Gets the node's integer value.
	 * @return The node's integer value.
	 */
	int getValue();

	/**
	 * Adds a child node.
	 * @param child The child node to add.
	 * <p>
	 * Note: The actual child is added, not just a copy.
	 * </p>
	 */
	void addChild(INode child);

	/**
	 * Adds child nodes.
	 * @param children The child nodes to add.
	 * <p>
	 * Note: The actual children are added, not just copies.
	 * </p>
	 */
	void addChildren(INode[] children);

	/**
	 * Checks if the object is equal to the node.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the node.
	 */
	@Override
	boolean equals(Object obj);
}
