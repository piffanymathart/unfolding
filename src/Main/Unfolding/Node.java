package Unfolding;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An integer-valued node with pointers to its parent (possibly null) and its children (possibly empty).
 */
public class Node implements INode {

	/**
	 * The inner node that implements most of the node functionality. Our node class is simply a wrapper.
	 */
	private InnerNode m_inner;

	/**
	 * Constructs a node containing the given value with neither parent or children.
	 * @param value The integer value.
	 */
	public Node(int value) {
		m_inner = new InnerNode(value);
	}

	/**
	 * Constructor from string.
	 * @param s The string representation.
	 */
	public Node(String s) {

		if (!s.contains("(")) {
			m_inner = new InnerNode(Integer.valueOf(s));
			return;
		}

		int openInd = s.indexOf("(");
		m_inner = new InnerNode(Integer.valueOf(s.substring(0, openInd)));

		// parse what's between the brackets
		String[] childStrs = new TreeReader().splitIntoChildStrings(s.substring(openInd + 1, s.length() - 1));
		for (String cs : childStrs) {
			addChild(new Node(cs));
		}
	}

	/**
	 * Copy constructor.
	 * @param node The node to copy from.
	 */
	public Node(INode node) {
		this(node.toString());
	}

	/**
	 * Gets the parent node (a shallow copy).
	 * @return The parent node.
	 */
	public INode getParent() {
		InnerNode innerParent = m_inner.getParent();
		return innerParent==null ? null : new Node(innerParent);
	}

	/**
	 * Gets the list of child nodes (shallow copies).
	 * @return The list of child nodes.
	 */
	public List<INode> getChildren() {
		List<InnerNode> innerChildren = m_inner.getChildren();
		List<INode> children = new ArrayList<>();
		for(InnerNode innerChild : innerChildren) {
			children.add(new Node(innerChild));
		}
		return children;
	}

	/**
	 * Gets the node's integer value.
	 * @return The node's integer value.
	 */
	public int getValue() {
		return m_inner.getValue();
	}

	/**
	 * Adds a child node.
	 * @param child The child node to add.
	 * <p>
	 * Note: The actual child is added, not just a copy.
	 * </p>
	 */
	public void addChild(INode child) {
		if(child==null) {
			throw new RuntimeException("cannot add null child");
		}
		if(hasChild(child)) {
			throw new RuntimeException("cannot add existing child");
		}
		m_inner.add(((Node)child).getInnerNode());
	}

	/**
	 * Adds child nodes.
	 * @param children The child nodes to add.
	 * <p>
	 * Note: The actual children are added, not just copies.
	 * </p>
	 */
	public void addChildren(INode[] children) {
		if(children==null) {
			throw new RuntimeException("cannot add null children");
		}
		for (INode child : children) addChild(child);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append(getValue());
		List<INode> children = getChildren();
		int n = children.size();
		if(n > 0) {
			sb.append("(");
			for (int i=0; i<n; i++) {
				sb.append(children.get(i).toString());
				if(i < n-1) sb.append(",");
			}
			sb.append(")");
		}
		return sb.toString();
	}

	/**
	 * Checks if the object is equal to the node.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the node.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!INode.class.isAssignableFrom(obj.getClass())) return false;
		return equals((INode) obj);
	}

	/**
	 * Checks if the two nodes are equal.
	 * @param node The node with which to compare.
	 * @return Whether the nodes are equal.
	 */
	private boolean equals(INode node) {
		return toString().equals(node.toString());
	}

	/**
	 * Constructor from inner node.
	 * @param inner The inner node.
	 */
	private Node(InnerNode inner) {
		this(inner.getValue());
		setInnerNode(inner);
	}

	/**
	 * Checks whether or not the specified node is a child of the current node.
	 * @param child The child node to check for.
	 * @return Whether or not the specified node is a child of the current node.
	 */
	private boolean hasChild(INode child) {
		List<INode> children = getChildren();
		for(INode c : children) {
			if(c.getValue() == child.getValue()) return true;
		}
		return false;
	}

	/**
	 * Gets the inner node.
	 * @return The inner node.
	 */
	private InnerNode getInnerNode() {
		return m_inner;
	}

	/**
	 * Sets the inner node.
	 * @param innerNode The inner node to set to.
	 */
	private void setInnerNode(InnerNode innerNode) {
		m_inner = innerNode;
	}

	/**
	 * Wrapper class for inner tree node.
	 */
	private class InnerNode extends DefaultMutableTreeNode {

		InnerNode(int val) {
			super();
			setUserObject(val);
		}

		public int getValue() {
			return (int) super.getUserObject();
		}

		public InnerNode getParent() {
			return (InnerNode) super.getParent();
		}

		public List<InnerNode> getChildren() {
			Enumeration<TreeNode> baseChildren = super.children();
			List<InnerNode> children = new ArrayList<>();
			while(baseChildren.hasMoreElements()) {
				children.add((InnerNode)baseChildren.nextElement());
			}
			return children;
		}
	}

	/**
	 * A helper class for reading tree structures from their string representations.
	 */
	private class TreeReader {
		/**
		 * Splits a string representing a node into child strings representing the child nodes.
		 * @param s0 The string.
		 * @return The strings corresponding to the children of the node.
		 * <p>
		 * Note: The string is of the form "a(b), c, d(e,f(g))"
		 * </p>
		 */
		private String[] splitIntoChildStrings(String s0) {
			String s = s0.replaceAll("\\s", "");
			List<String> childStrs = new ArrayList<>();
			while (!s.isEmpty()) {
				// consume initial comma, if exists
				if (s.startsWith(",")) s = s.substring(1);
				// consume value
				int firstNonnumericInd = findFirstNonnumeric(s);
				String cs = s.substring(0, firstNonnumericInd);
				s = s.substring(firstNonnumericInd);
				// consume content inside brackets
				if (s.startsWith("(")) {
					int closeInd = findMatchingClosingBracket(s);
					cs += s.substring(0, closeInd + 1);
					s = s.substring(closeInd + 1);
				}
				childStrs.add(cs);
			}
			return childStrs.toArray(new String[childStrs.size()]);
		}

		/**
		 * Finds the index of the first nonnumeric value in the string.
		 * @param s The string.
		 * @return The index of the first nonnumeric value.
		 */
		private int findFirstNonnumeric(String s) {
			Pattern numericPat = Pattern.compile("[\\d]+");
			Matcher matcher = numericPat.matcher(s);
			if (!matcher.find()) throw new RuntimeException();
			return matcher.end();
		}

		/**
		 * Finds the index of the matching closing bracket for a string that starts with an opening bracket.
		 * @param s A string that starts with an opening brackets.
		 * @return The index of the matching closing bracket.
		 */
		private int findMatchingClosingBracket(String s) {
			int surplus = 1;
			int n = s.length();
			for (int i = 1; i < n; i++) {
				switch (s.charAt(i)) {
					case '(':
						surplus++;
						break;
					case ')':
						surplus--;
						break;
				}
				if (surplus == 0) return i;
			}
			throw new RuntimeException();
		}
	}
}
