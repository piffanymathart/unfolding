package Unfolding;

import Geometry.Polygons.IPolygon2d;

import javax.vecmath.Point2d;

class BoundingBoxHelper {

	/**
	 * Gets the bounding box of a set of polygons.
	 * @param polygons The set of polygons.
	 * @return Their bounding box.
	 */
	static double[] getBoundingBox(IPolygon2d[] polygons) {

		// array of minX, minY, minZ, maxX, maxY, maxZ
		Point2d minPt = null;
		Point2d maxPt = null;

		// get min-max values from faces
		if(polygons!=null) {
			for (IPolygon2d face : polygons) {
				for (int i = 0; i < face.size(); i++) {
					Point2d p = face.get(i);
					minPt = min(minPt, p);
					maxPt = max(maxPt, p);
				}
			}
		}

		if(minPt==null || maxPt==null) throw new RuntimeException();

		return new double[] {minPt.x, maxPt.x, minPt.y, maxPt.y};
	}

	/**
	 * Computes the minimum of two points.
	 * @param p1 The first point.
	 * @param p2 The second point.
	 * @return Their minimum.
	 */
	private static Point2d min(Point2d p1, Point2d p2) {
		if(p1 == null) return p2;
		if(p2 == null) return p1;
		return new Point2d(
			Math.min(p1.x, p2.x),
			Math.min(p1.y, p2.y)
		);
	}

	/**
	 * Computes the maximum of two points.
	 * @param p1 The first point.
	 * @param p2 The second point.
	 * @return Their maximum.
	 */
	private static Point2d max(Point2d p1, Point2d p2) {
		if(p1 == null) return p2;
		if(p2 == null) return p1;
		return new Point2d(
			Math.max(p1.x, p2.x),
			Math.max(p1.y, p2.y)
		);
	}
}
