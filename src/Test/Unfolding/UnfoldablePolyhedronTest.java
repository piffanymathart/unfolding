package Unfolding;

import Geometry.Polygons.IPolygon2d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon2d;
import Geometry.Polygons.Polygon3d;
import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.*;

class UnfoldablePolyhedronTest {

	@Test
	void defaultConstructor() {
		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron();
		assertEquals(0, polyhedron.getVertices().length);
		assertEquals(0, polyhedron.getFaceIndices().length);
		assertNull(polyhedron.getNetTree());
	}

	@Test
	void copyConstructor() {
		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron();
		IUnfoldablePolyhedron copy = new UnfoldablePolyhedron(polyhedron);
		assertArrayEquals(polyhedron.getVertices(), copy.getVertices());
		assertArrayEquals(polyhedron.getFaceIndices(), copy.getFaceIndices());
		assertEquals(polyhedron.getNetTree(), copy.getNetTree());
	}

	@Test
	void valuesConstructor() {
		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};
		INode netTree = new Node("0(2,1)");

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(vertices, new int[0][0], faceIndices, null, netTree);
		assertArrayEquals(vertices, polyhedron.getVertices());
		assertArrayEquals(faceIndices, polyhedron.getFaceIndices());
		assertEquals(netTree, polyhedron.getNetTree());
	}

	@Test
	void polyTreeConstructor() {
		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};
		INode netTree = new Node("0(2,1)");
		IColouredPolyhedron polyhedron = new ColouredPolyhedron(vertices, new int[0][0], faceIndices, null);

		IUnfoldablePolyhedron unfoldablePolyhedron = new UnfoldablePolyhedron(polyhedron, netTree);
		assertEquals(polyhedron, unfoldablePolyhedron);
		assertEquals(netTree, unfoldablePolyhedron.getNetTree());
	}


	@Test
	void getNetTree() {

		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};
		INode netTree = new Node("0(2,1)");

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(vertices, new int[0][0], faceIndices, null, netTree);
		assertEquals(netTree, polyhedron.getNetTree());
	}

	@Test
	void setNetTree() {

		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(vertices, new int[0][0], faceIndices, null, null);
		assertNull(polyhedron.getNetTree());

		INode netTree = new Node("0(2,1)");
		polyhedron.setNetTree(netTree);
		assertEquals(netTree, polyhedron.getNetTree());
	}

	@Test
	void equals() {

		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices1 = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};
		int[][] faceIndices2 = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,0}};

		IColouredPolyhedron polyhedron1 = new ColouredPolyhedron(vertices, null, faceIndices1, null);
		IColouredPolyhedron polyhedron2 = new ColouredPolyhedron(vertices, null, faceIndices2, null);
		IColouredPolyhedron[] polyhedra = {polyhedron1, polyhedron2};

		INode tree1 = new Node("0(2,1)");
		INode tree2 = new Node("0(1,2)");
		INode[] trees = {tree1, tree2, null};

		IUnfoldablePolyhedron polyhedron00 = new UnfoldablePolyhedron(polyhedra[0], trees[0]);
		for(int i=0; i<polyhedra.length; i++) {
			for(int j=0; j<trees.length; j++) {
				IUnfoldablePolyhedron polyhedronij = new UnfoldablePolyhedron(
					polyhedra[i],
					trees[j]
				);
				assertEquals(i==0 && j==0, polyhedron00.equals(polyhedronij));
			}
		}
	}

	@Test
	void equals_nullTree() {

		Point3d[] vertices = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] faceIndices1 = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};
		int[][] faceIndices2 = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,0}};

		IColouredPolyhedron polyhedron1 = new ColouredPolyhedron(vertices, null, faceIndices1, null);
		IColouredPolyhedron polyhedron2 = new ColouredPolyhedron(vertices, null, faceIndices2, null);
		IColouredPolyhedron[] polyhedra = {polyhedron1, polyhedron2};

		INode tree1 = new Node("0(2,1)");
		INode tree2 = new Node("0(1,2)");
		INode[] trees = {null, tree1, tree2};

		IUnfoldablePolyhedron polyhedron00 = new UnfoldablePolyhedron(polyhedra[0], trees[0]);
		for(int i=0; i<polyhedra.length; i++) {
			for(int j=0; j<trees.length; j++) {
				IUnfoldablePolyhedron polyhedronij = new UnfoldablePolyhedron(
					polyhedra[i],
					trees[j]
				);
				assertEquals(i==0 && j==0, polyhedron00.equals(polyhedronij));
			}
		}
	}

	@Test
	void getUnfoldingTransforms_cube_fitToWidth() {

		int w = 4;
		int h = 5;
		IUnfoldablePolyhedron polyhedron = createCube();

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));
		mat.rotX(4);
		mat.setScale(5);
		polyhedron.transform(mat);

		Matrix4d[] unfoldTransforms = polyhedron.getUnfoldingTransforms(w, h);

		IPolygon3d[] newFaces = new IPolygon3d[6];
		for(int i=0; i<6; i++) {
			IPolygon3d transformedFace = applyTransform(
				polyhedron.getFace(i),
				unfoldTransforms[i]
			);
			newFaces[i] = new Polygon3d(roundValues(transformedFace.getVertices(),1e-5));
		}

		IPolygon3d expectedBottom = new Polygon3d(
			new Point3d(2,3,0),
			new Point3d(3,3,0),
			new Point3d(3,2,0),
			new Point3d(2,2,0)
		);

		IPolygon3d expectedLeft = new Polygon3d(
			new Point3d(2,3,0),
			new Point3d(2,2,0),
			new Point3d(1,2,0),
			new Point3d(1,3,0)
		);

		IPolygon3d expectedRight = new Polygon3d(
			new Point3d(3,3,0),
			new Point3d(4,3,0),
			new Point3d(4,2,0),
			new Point3d(3,2,0)
		);

		IPolygon3d expectedNear = new Polygon3d(
			new Point3d(3,2,0),
			new Point3d(3,1,0),
			new Point3d(2,1,0),
			new Point3d(2,2,0)
		);

		IPolygon3d expectedFar = new Polygon3d(
			new Point3d(2,3,0),
			new Point3d(2,4, 0),
			new Point3d(3,4,0),
			new Point3d(3,3,0)
		);

		IPolygon3d expectedTop = new Polygon3d(
			new Point3d(1,3,0),
			new Point3d(1,2, 0),
			new Point3d(0,2,0),
			new Point3d(0,3,0)
		);

		assertEqualVertices(newFaces[0], expectedBottom, 1e-5);
		assertEqualVertices(newFaces[1], expectedLeft, 1e-5);
		assertEqualVertices(newFaces[2], expectedRight, 1e-5);
		assertEqualVertices(newFaces[3], expectedNear, 1e-5);
		assertEqualVertices(newFaces[4], expectedFar, 1e-5);
		assertEqualVertices(newFaces[5], expectedTop, 1e-5);
	}

	@Test
	void getUnfoldingTransforms_cube_fitToHeight() {

		int w = 6;
		int h = 3;
		IUnfoldablePolyhedron polyhedron = createCube();

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));
		mat.rotX(4);
		mat.setScale(5);
		polyhedron.transform(mat);

		Matrix4d[] unfoldTransforms = polyhedron.getUnfoldingTransforms(w, h);

		IPolygon3d[] newFaces = new IPolygon3d[6];
		for(int i=0; i<6; i++) {
			IPolygon3d transformedFace = applyTransform(
				polyhedron.getFace(i),
				unfoldTransforms[i]
			);
			newFaces[i] = new Polygon3d(roundValues(transformedFace.getVertices(),1e-5));
		}

		IPolygon3d expectedBottom = new Polygon3d(
			new Point3d(3,2,0),
			new Point3d(4,2,0),
			new Point3d(4,1,0),
			new Point3d(3,1,0)
		);

		IPolygon3d expectedLeft = new Polygon3d(
			new Point3d(3,2,0),
			new Point3d(3,1,0),
			new Point3d(2,1,0),
			new Point3d(2,2,0)
		);

		IPolygon3d expectedRight = new Polygon3d(
			new Point3d(4,2,0),
			new Point3d(5,2,0),
			new Point3d(5,1,0),
			new Point3d(4,1,0)
		);

		IPolygon3d expectedNear = new Polygon3d(
			new Point3d(4,1,0),
			new Point3d(4,0,0),
			new Point3d(3,0,0),
			new Point3d(3,1,0)
		);

		IPolygon3d expectedFar = new Polygon3d(
			new Point3d(3,2,0),
			new Point3d(3,3, 0),
			new Point3d(4,3,0),
			new Point3d(4,2,0)
		);

		IPolygon3d expectedTop = new Polygon3d(
			new Point3d(2,2,0),
			new Point3d(2,1, 0),
			new Point3d(1,1,0),
			new Point3d(1,2,0)
		);

		assertEqualVertices(newFaces[0], expectedBottom, 1e-5);
		assertEqualVertices(newFaces[1], expectedLeft, 1e-5);
		assertEqualVertices(newFaces[2], expectedRight, 1e-5);
		assertEqualVertices(newFaces[3], expectedNear, 1e-5);
		assertEqualVertices(newFaces[4], expectedFar, 1e-5);
		assertEqualVertices(newFaces[5], expectedTop, 1e-5);
	}

	@Test
	void getUnfolding_cube_fitToWidth() {

		int w = 4;
		int h = 5;
		IUnfoldablePolyhedron polyhedron = createCube();

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));
		mat.rotX(4);
		mat.setScale(5);
		polyhedron.transform(mat);

		IPolygon2d[] newFaces = polyhedron.getUnfolding(w, h);

		IPolygon2d expectedBottom = new Polygon2d(
			new Point2d(2,3),
			new Point2d(3,3),
			new Point2d(3,2),
			new Point2d(2,2)
		);

		IPolygon2d expectedLeft = new Polygon2d(
			new Point2d(2,3),
			new Point2d(2,2),
			new Point2d(1,2),
			new Point2d(1,3)
		);

		IPolygon2d expectedRight = new Polygon2d(
			new Point2d(3,3),
			new Point2d(4,3),
			new Point2d(4,2),
			new Point2d(3,2)
		);

		IPolygon2d expectedNear = new Polygon2d(
			new Point2d(3,2),
			new Point2d(3,1),
			new Point2d(2,1),
			new Point2d(2,2)
		);

		IPolygon2d expectedFar = new Polygon2d(
			new Point2d(2,3),
			new Point2d(2,4),
			new Point2d(3,4),
			new Point2d(3,3)
		);

		IPolygon2d expectedTop = new Polygon2d(
			new Point2d(1,3),
			new Point2d(1,2),
			new Point2d(0,2),
			new Point2d(0,3)
		);

		assertEqualVertices(newFaces[0], expectedBottom, 1e-5);
		assertEqualVertices(newFaces[1], expectedLeft, 1e-5);
		assertEqualVertices(newFaces[2], expectedRight, 1e-5);
		assertEqualVertices(newFaces[3], expectedNear, 1e-5);
		assertEqualVertices(newFaces[4], expectedFar, 1e-5);
		assertEqualVertices(newFaces[5], expectedTop, 1e-5);
	}
	
	@Test
	void getUnfolding_cube_fitToHeight() {

		int w = 6;
		int h = 3;
		IUnfoldablePolyhedron polyhedron = createCube();

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(1,2,3));
		mat.rotX(4);
		mat.setScale(5);
		polyhedron.transform(mat);

		IPolygon2d[] newFaces = polyhedron.getUnfolding(w, h);

		IPolygon2d expectedBottom = new Polygon2d(
			new Point2d(3,2),
			new Point2d(4,2),
			new Point2d(4,1),
			new Point2d(3,1)
		);

		IPolygon2d expectedLeft = new Polygon2d(
			new Point2d(3,2),
			new Point2d(3,1),
			new Point2d(2,1),
			new Point2d(2,2)
		);

		IPolygon2d expectedRight = new Polygon2d(
			new Point2d(4,2),
			new Point2d(5,2),
			new Point2d(5,1),
			new Point2d(4,1)
		);

		IPolygon2d expectedNear = new Polygon2d(
			new Point2d(4,1),
			new Point2d(4,0),
			new Point2d(3,0),
			new Point2d(3,1)
		);

		IPolygon2d expectedFar = new Polygon2d(
			new Point2d(3,2),
			new Point2d(3,3),
			new Point2d(4,3),
			new Point2d(4,2)
		);

		IPolygon2d expectedTop = new Polygon2d(
			new Point2d(2,2),
			new Point2d(2,1),
			new Point2d(1,1),
			new Point2d(1,2)
		);

		assertEqualVertices(newFaces[0], expectedBottom, 1e-5);
		assertEqualVertices(newFaces[1], expectedLeft, 1e-5);
		assertEqualVertices(newFaces[2], expectedRight, 1e-5);
		assertEqualVertices(newFaces[3], expectedNear, 1e-5);
		assertEqualVertices(newFaces[4], expectedFar, 1e-5);
		assertEqualVertices(newFaces[5], expectedTop, 1e-5);
	}

	@Test
	void forCoverageSake() {
		new FitHelper();
		new BoundingBoxHelper();
	}

	private IUnfoldablePolyhedron createCube() {

		Point3d[] vertices = {
			new Point3d(0,0,0), // bottom left far = 0
			new Point3d(1,0,0), // bottom right far = 1
			new Point3d(1,0,1), // bottom right near = 2
			new Point3d(0,0,1), // bottom left near = 3
			new Point3d(0,1,0), // top left far = 4
			new Point3d(1,1,0), // top right far = 5
			new Point3d(1,1,1), // top right near = 6
			new Point3d(0,1,1), // top left near = 7
		};

		int[][] faceIndices = {
			{0,1,2,3}, // bottom = 0
			{0,3,7,4}, // left = 1
			{1,5,6,2}, // right = 2
			{2,6,7,3}, // near = 3
			{0,4,5,1}, // far = 4
			{4,7,6,5} // top = 5
		};

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);
		INode top = new Node(5);

		left.addChild(top);
		bottom.addChildren(new INode[] {left,far,right,near});

		INode netTree = createCubeUnfolding();

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(
			vertices, new int[0][0], faceIndices, null, netTree
		);

		return polyhedron;
	}

	private INode createCubeUnfolding() {

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);
		INode top = new Node(5);

		left.addChild(top);
		bottom.addChildren(new INode[] {left,far,right,near});

		return bottom;
	}

	private void assertEqualVertices(IPolygon3d poly1, IPolygon3d poly2, double tol) {
		Point3d[] v1 = poly1.getVertices();
		Point3d[] v2 = poly2.getVertices();
		if(v1.length != v2.length) assert(false);
		v1 = roundValues(v1, tol);
		v2 = roundValues(v2, tol);
		assertArrayEquals(v1,v2);
	}

	private void assertEqualVertices(IPolygon2d poly1, IPolygon2d poly2, double tol) {
		Point2d[] v1 = poly1.getVertices();
		Point2d[] v2 = poly2.getVertices();
		if(v1.length != v2.length) assert(false);
		v1 = roundValues(v1, tol);
		v2 = roundValues(v2, tol);
		assertArrayEquals(v1,v2);
	}

	private Point3d[] roundValues(Point3d[] points, double tol) {

		int n = points.length;
		double factor = Math.round(1.0/tol);
		Point3d[] newPts = new Point3d[n];
		for(int i=0; i<n; i++) {
			double x = points[i].x;
			double y = points[i].y;
			double z = points[i].z;
			double newX = Math.round(x*factor)/factor;
			double newY = Math.round(y*factor)/factor;
			double newZ = Math.round(z*factor)/factor;
			newPts[i] = new Point3d(newX, newY, newZ);
		}
		return newPts;
	}

	private Point2d[] roundValues(Point2d[] points, double tol) {

		int n = points.length;
		double factor = Math.round(1.0/tol);
		Point2d[] newPts = new Point2d[n];
		for(int i=0; i<n; i++) {
			double x = points[i].x;
			double y = points[i].y;
			double newX = Math.round(x*factor)/factor;
			double newY = Math.round(y*factor)/factor;
			newPts[i] = new Point2d(newX, newY);
		}
		return newPts;
	}

	private IPolygon3d applyTransform(IPolygon3d polygon, Matrix4d matrix) {
		int n = polygon.size();
		Point3d[] verts = new Point3d[n];
		for(int i=0; i<n; i++) {
			verts[i] = applyTransform(polygon.get(i), matrix);
		}
		return new Polygon3d(verts);
	}

	private Point3d applyTransform(Point3d p, Matrix4d matrix) {
		Point3d newPt = new Point3d(p);
		matrix.transform(newPt);
		return newPt;
	}
}