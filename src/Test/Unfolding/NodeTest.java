package Unfolding;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {

	@Test
	void valueConstructor() {

		INode node = new Node(2);
		assertEquals(2, node.getValue());
		assertTrue(node.getChildren().isEmpty());
		assertNull(node.getParent());
	}

	@Test
	void stringConstructor() {

		INode n125 = new Node(125);
		n125.addChild(new Node(73));

		INode n6 = new Node(6);
		n6.addChild(new Node(9));
		INode n3 = new Node(3);
		INode n4 = new Node(4);
		n4.addChildren(new INode[] {n3, n6});

		INode n1 = new Node(1);
		n1.addChild(new Node(10));
		INode n5 = new Node(5);
		n5.addChild(n1);

		INode expected = new Node(82);
		expected.addChildren(new INode[] {n125, n4, n5});

		INode actual = new Node("82(   125 (73) ,  4(3,6(  9)) ,5(1( 10)))");

		assertEquals(expected, actual);
	}

	@Test
	void stringConstructor_noMatchingBrackerException() {

		assertThrows(
			RuntimeException.class,
			() -> new Node("82(   125 (73) ,  4(3,6(  9)) ,5(1( 10))")
		);
	}

	@Test
	void copyConstructor() {

		INode node1 = new Node("82(   125 (73) ,  4(3,6(  9)) ,5(1( 10)))");
		INode node2 = new Node(node1);
		assertEquals(node1, node2);
		// check that the copies are not linked
		node1.addChild(new Node(33));
		assertNotEquals(node1, node2);
	}

	@Test
	void getParent() {

		INode child1 = new Node(1);
		INode child2 = new Node(2);
		INode[] children = {child1, child2};

		INode node = new Node(3);
		node.addChildren(children);

		INode parent1 = child1.getParent();
		INode parent2 = child2.getParent();

		assertEquals(parent1, parent2);

		// check that the two copies are linked
		parent1.addChild(new Node(33));
		assertEquals(parent1, parent2);

		assertNull(parent1.getParent());
	}

	@Test
	void getChildren() {
		{
			INode[] children = {};
			INode node = new Node(3);
			node.addChildren(children);
			List<INode> actual = node.getChildren();
			assertArrayEquals(children, actual.toArray(new INode[actual.size()]));
		}
		{
			INode[] children= {new Node(4)};
			INode node = new Node(3);
			node.addChildren(children);
			List<INode> actual = node.getChildren();
			assertArrayEquals(children, actual.toArray(new INode[actual.size()]));
		}
		{
			INode[] children = {new Node(4), new Node(5)};
			INode node = new Node(3);
			node.addChildren(children);
			List<INode> actual = node.getChildren();
			assertArrayEquals(children, actual.toArray(new INode[actual.size()]));
		}
	}

	@Test
	void getValue() {
		INode node = new Node("82(   125 (73) ,  4(3,6(  9)) ,5(1( 10)))");
		assertEquals(82, node.getValue());
	}

	@Test
	void addChild() {
		INode root = new Node(0);
		root.addChild(new Node(1));
		root.addChild(new Node(2));
		assertEquals(2, root.getChildren().size());
	}

	@Test
	void addChild_nullException() {
		INode root = new Node(0);
		assertThrows(RuntimeException.class, () -> root.addChild(null));
	}

	@Test
	void addChild_duplicateException() {
		INode root = new Node(0);
		root.addChild(new Node(1));
		assertThrows(RuntimeException.class, () -> root.addChild(new Node(1)));
	}

	@Test
	void addChildren() {
		INode root = new Node(0);
		root.addChildren(new Node[] {new Node(1), new Node(2)});
		assertEquals(2, root.getChildren().size());
	}

	@Test
	void addChildren_nullException() {
		INode root = new Node(0);
		assertThrows(RuntimeException.class, () -> root.addChildren(null));
	}

	@Test
	void equals() {
		int[] vals = {1, 2};
		INode[][] childrenSets = {
			{new Node("1"), new Node("2(4,5)")},
			{},
			{new Node("1")},
			{new Node("1"), new Node("3")},
			{new Node("1"), new Node("2")},
			{new Node("1"), new Node("2(4,6)")}
		};

		int numChildrenTypes = 6;

		INode node = new Node(vals[0]);
		node.addChildren(childrenSets[0]);

		for(int i=0; i<vals.length; i++) {
			for(int j=0; j<numChildrenTypes; j++) {
				INode other = new Node(vals[i]);
				other.addChildren(deepCopy(childrenSets[j]));
				if(i==0 && j==0) assertEquals(node, other);
				else assertNotEquals(node, other);
			}
		}
	}

	@Test
	void toStringTest() {
		INode n125 = new Node(125);
		n125.addChild(new Node(73));

		INode n6 = new Node(6);
		n6.addChild(new Node(9));
		INode n3 = new Node(3);
		INode n4 = new Node(4);
		n4.addChildren(new INode[] {n3, n6});

		INode n1 = new Node(1);
		n1.addChild(new Node(10));
		INode n5 = new Node(5);
		n5.addChild(n1);

		INode node = new Node(82);
		node.addChildren(new INode[] {n125, n4, n5});

		String expected = "82(125(73),4(3,6(9)),5(1(10)))";
		String actual = node.toString();

		assertEquals(expected, actual);
	}

	private INode[] deepCopy(INode[] children) {
		int n = children.length;
		INode[] copy = new INode[n];
		for(int i=0; i<n; i++) {
			copy[i] = new Node(children[i]);
		}
		return copy;
	}
}