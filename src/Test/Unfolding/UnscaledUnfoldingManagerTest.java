package Unfolding;

import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import Polyhedron.IPolyhedron;
import Polyhedron.Polyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class UnscaledUnfoldingManagerTest {

	@Test
	void getUnfoldingTransforms_triangularPrism() {
		IPolyhedron polyhedron = createTriangularPrism();
		INode netTree = createTriangularPrismUnfolding();
		Matrix4d[] unfoldTransforms = new UnscaledUnfoldingManager()
			.getUnfoldingTransforms(polyhedron, netTree);

		IPolygon3d[] newFaces = new IPolygon3d[5];
		for(int i=0; i<5; i++) {
			IPolygon3d transformedFace = applyTransform(
				polyhedron.getFace(i),
				unfoldTransforms[i]
			);
			newFaces[i] = new Polygon3d(roundValues(transformedFace.getVertices(),1e-5));
		}

		IPolygon3d expectedBottom = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(1,0,0),
			new Point3d(1,-1,0),
			new Point3d(0,-1,0)
		);

		IPolygon3d expectedLeft = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(0,-1,0),
			new Point3d(-5.0/6.0,-1,0),
			new Point3d(-5.0/6.0,0,0)
		);

		IPolygon3d expectedRight = new Polygon3d(
			new Point3d(-10.0/6.0,0,0),
			new Point3d(-5.0/6.0,0,0),
			new Point3d(-5.0/6.0,-1,0),
			new Point3d(-10.0/6.0,-1,0)
		);

		IPolygon3d expectedNear = new Polygon3d(
			new Point3d(1,-1,0),
			new Point3d(0.5,-1.0-4.0/6.0,0),
			new Point3d(0,-1,0)
		);

		IPolygon3d expectedFar = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(0.5,4.0/6.0,0),
			new Point3d(1,0,0)
		);

		assertEqualVertices(expectedBottom, newFaces[0], 1e-5);
		assertEqualVertices(expectedLeft, newFaces[1], 1e-5);
		assertEqualVertices(expectedRight, newFaces[2], 1e-5);
		assertEqualVertices(expectedNear, newFaces[3], 1e-5);
		assertEqualVertices(expectedFar, newFaces[4], 1e-5);
	}

	@Test
	void getUnfoldingTransforms_cube() {
		IPolyhedron polyhedron = createCube();
		INode netTree = createCubeUnfolding();
		Matrix4d[] unfoldTransforms = new UnscaledUnfoldingManager()
			.getUnfoldingTransforms(polyhedron, netTree);

		IPolygon3d[] newFaces = new IPolygon3d[6];
		for(int i=0; i<6; i++) {
			IPolygon3d transformedFace = applyTransform(
				polyhedron.getFace(i),
				unfoldTransforms[i]
			);
			newFaces[i] = new Polygon3d(roundValues(transformedFace.getVertices(),1e-5));
		}

		IPolygon3d expectedBottom = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(1,0,0),
			new Point3d(1,-1,0),
			new Point3d(0,-1,0)
		);

		IPolygon3d expectedLeft = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(0,-1,0),
			new Point3d(-1,-1,0),
			new Point3d(-1,0,0)
		);

		IPolygon3d expectedRight = new Polygon3d(
			new Point3d(1,0,0),
			new Point3d(2,0,0),
			new Point3d(2,-1,0),
			new Point3d(1,-1,0)
		);

		IPolygon3d expectedNear = new Polygon3d(
			new Point3d(1,-1,0),
			new Point3d(1,-2,0),
			new Point3d(0,-2,0),
			new Point3d(0,-1,0)
		);

		IPolygon3d expectedFar = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(0,1, 0),
			new Point3d(1,1,0),
			new Point3d(1,0,0)
		);

		IPolygon3d expectedTop = new Polygon3d(
			new Point3d(-1,0,0),
			new Point3d(-1,-1, 0),
			new Point3d(-2,-1,0),
			new Point3d(-2,0,0)
		);

		assertEqualVertices(newFaces[0], expectedBottom, 1e-5);
		assertEqualVertices(newFaces[1], expectedLeft, 1e-5);
		assertEqualVertices(newFaces[2], expectedRight, 1e-5);
		assertEqualVertices(newFaces[3], expectedNear, 1e-5);
		assertEqualVertices(newFaces[4], expectedFar, 1e-5);
		assertEqualVertices(newFaces[5], expectedTop, 1e-5);
	}

	private IPolyhedron createCube() {

		Point3d[] vertices = {
			new Point3d(0,0,0), // bottom left far = 0
			new Point3d(1,0,0), // bottom right far = 1
			new Point3d(1,0,1), // bottom right near = 2
			new Point3d(0,0,1), // bottom left near = 3
			new Point3d(0,1,0), // top left far = 4
			new Point3d(1,1,0), // top right far = 5
			new Point3d(1,1,1), // top right near = 6
			new Point3d(0,1,1), // top left near = 7
		};

		int[][] faceIndices = {
			{0,1,2,3}, // bottom = 0
			{0,3,7,4}, // left = 1
			{1,5,6,2}, // right = 2
			{2,6,7,3}, // near = 3
			{0,4,5,1}, // far = 4
			{4,7,6,5} // top = 5
		};

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);
		INode top = new Node(5);

		left.addChild(top);
		bottom.addChildren(new INode[] {left,far,right,near});

		return new Polyhedron(
			vertices, new int[0][0], faceIndices
		);
	}

	private INode createCubeUnfolding() {

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);
		INode top = new Node(5);

		left.addChild(top);
		bottom.addChildren(new INode[] {left,far,right,near});

		return bottom;
	}

	private IPolyhedron createTriangularPrism() {

		Point3d[] vertices = {
			new Point3d(0,0,0), // bottom left far = 0
			new Point3d(1,0,0), // bottom right far = 1
			new Point3d(1,0,1), // bottom right near = 2
			new Point3d(0,0,1), // bottom left near = 3
			new Point3d(0.5,0.4/0.6,0), // top far = 4
			new Point3d(0.5,0.4/0.6,1), // top near = 5
		};

		int[][] faceIndices = {
			{0,1,2,3}, // bottom = 0
			{0,3,5,4}, // left = 1
			{1,4,5,2}, // right = 2
			{2,5,3}, // near = 3
			{0,4,1} // far = 4
		};

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);

		left.addChild(right);
		bottom.addChildren(new INode[] {left,far,near});

		return new Polyhedron(
			vertices, new int[0][0], faceIndices
		);
	}

	private INode createTriangularPrismUnfolding() {

		INode bottom = new Node(0);
		INode left = new Node(1);
		INode right = new Node(2);
		INode near = new Node(3);
		INode far = new Node(4);

		left.addChild(right);
		bottom.addChildren(new INode[] {left,far,near});

		return bottom;
	}

	private void assertEqualVertices(IPolygon3d poly1, IPolygon3d poly2, double tol) {
		Point3d[] v1 = poly1.getVertices();
		Point3d[] v2 = poly2.getVertices();
		if(v1.length != v2.length) assert(false);
		v1 = roundValues(v1, tol);
		v2 = roundValues(v2, tol);
		assertArrayEquals(v1,v2);
	}

	private Point3d[] roundValues(Point3d[] points, double tol) {

		int n = points.length;
		double factor = Math.round(1.0/tol);
		Point3d[] newPts = new Point3d[n];
		for(int i=0; i<n; i++) {
			double x = points[i].x;
			double y = points[i].y;
			double z = points[i].z;
			double newX = Math.round(x*factor)/factor;
			double newY = Math.round(y*factor)/factor;
			double newZ = Math.round(z*factor)/factor;
			newPts[i] = new Point3d(newX, newY, newZ);
		}
		return newPts;
	}

	private IPolygon3d applyTransform(IPolygon3d polygon, Matrix4d matrix) {
		int n = polygon.size();
		Point3d[] verts = new Point3d[n];
		for(int i=0; i<n; i++) {
			verts[i] = applyTransform(polygon.get(i), matrix);
		}
		return new Polygon3d(verts);
	}

	private Point3d applyTransform(Point3d p, Matrix4d matrix) {
		Point3d newPt = new Point3d(p);
		matrix.transform(newPt);
		return newPt;
	}
}